﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	private Transform _myTransform;
	public float EnemySpeed;

	// Use this for initialization
	void Start () 
	{
		_myTransform = GetComponent<Transform>();		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	void FixedUpdate()
	{
		_myTransform.Translate(-(EnemySpeed * Time.deltaTime), 0, 0);
	}
}
