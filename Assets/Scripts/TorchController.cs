﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchController : MonoBehaviour, IUsable
{
    private SpriteRenderer _sr;
    private Animator anim;

    public bool IsActive
    {
        get { return anim.GetBool("isActive"); }
        set { anim.SetBool("isActive", value) ;}
    }

	// Use this for initialization
	void Start ()
	{
	    _sr = GetComponent<SpriteRenderer>();
	    anim = GetComponent<Animator>();
	    IsActive = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Use()
    {
        IsActive = true;
    }
}
