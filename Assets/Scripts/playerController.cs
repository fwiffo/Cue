﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour {

    public float MaxSpeed;
    public float JumpForce;
    private Rigidbody2D _playerRB;
    private bool _isGrounded = false;
    private float _groundCheckRadius = 0.01f;
    public LayerMask GroundLayer;
    public Transform GroundCheck;
    public Transform MeleeAttackPosition;
    public GameObject MeleeAttackZoneCont;
    private Animator _playerAnimator;

    private bool isFacingRight = true;

    // Use this for initialization
    void Start ()
	{
	    _playerRB = GetComponent<Rigidbody2D>();
        _playerAnimator = GetComponent<Animator>();
	}

    void Update()
    {
        if (_isGrounded && Input.GetAxis("Jump") > 0)
        {
            _isGrounded = false;
            //myAnim.SetBool("isGrounded", grounded);
            _playerRB.AddForce(new Vector2(0, JumpForce), ForceMode2D.Impulse);
        }
    }
	
	// Update is called once per frame
	void FixedUpdate ()
	{
	    float move = Input.GetAxis("Horizontal");
        _playerAnimator.SetFloat("speed", Mathf.Abs(move));
	    if (move < 0 && isFacingRight)
	    {
	        isFacingRight = false;
            flip();
	    }
        else if (move > 0 && !isFacingRight)
	    {
	        isFacingRight = true;
            flip();
	    }
	    if (Input.GetAxisRaw("Fire1") != 0)
	    {
	        MeleeAttack();
	    }
        _playerRB.velocity = new Vector2(move*MaxSpeed, _playerRB.velocity.y);

	    _isGrounded = Physics2D.OverlapCircle(GroundCheck.position, _groundCheckRadius, GroundLayer);
    }

    void MeleeAttack()
    {
        if (isFacingRight)
        {
            Instantiate(MeleeAttackZoneCont, MeleeAttackPosition.position, Quaternion.Euler(new Vector3(0, 0, 0)));
        }
        else if (!isFacingRight)
        {
            Instantiate(MeleeAttackZoneCont, MeleeAttackPosition.position, Quaternion.Euler(new Vector3(0, 0, 180f)));
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (Input.GetAxisRaw("Fire2") != 0)
        {
            GameObject obj = other.gameObject;
            if (obj.tag.Equals("Usable"))
            {
                IUsable usable = obj.GetComponent<IUsable>();
                usable.Use();
            }
        }
    }

    void flip()
	{
		Vector3 playerScale = transform.localScale;
		playerScale.x *= -1;
		transform.localScale = playerScale;
	}
}
