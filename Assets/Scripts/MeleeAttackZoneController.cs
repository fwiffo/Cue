﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttackZoneController : MonoBehaviour {

    private Rigidbody2D myRB;
    private BoxCollider2D collider2D;
    public float lifeTime;

    // Use this for initialization
    void Start()
    {

    }

    void Awake()
    {
        myRB = GetComponent<Rigidbody2D>();
        collider2D = GetComponent<BoxCollider2D>();
        Destroy(gameObject, lifeTime);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Attackable"))
        {
            Destroy(other.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
